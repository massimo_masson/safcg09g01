#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# safcg09g01:timesheet 
# rilevazione tempi pratiche
# Copyright (C) 2021 Massimo Masson
# 
# This program is dual-licensed.
# 
# Option 1:
# If you respect the terms of GNU GPL license, AND
# you agree to give the copyright for modifications or derivative work
# to the original author Massimo Masson, the GPL license applies.
# In this case:
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# 
# Option 2:
# If you do not agree with any of the statements in option 1, then
# a proprietary license applies. In this case, contact the author
# for a dedicated propietary license.
# 

def config(root,application=None):
    timesheet = root.branch('Timesheet')

    #
    # menu rilevazione pratiche
    #
    rilevazioni = timesheet.branch('!![it]Rilevazioni')

    rilevazioni.thpage('!![it]Gestione attività', table = 'timesheet.rilevazione')
    rilevazioni.thpage('!![it]Analisi', table = 'timesheet.rilevazione',
            viewResource='ViewAnalisiRilevazioni' #, formResource='FormIncarichi')
            )

    #
    # menu anagrafiche
    #
    anagrafiche = timesheet.branch('!![it]Anagrafiche')

    anagrafiche.thpage('!![it]Clienti', table = 'timesheet.anagrafica_cliente')
    anagrafiche.thpage('!![it]Pratiche', table = 'timesheet.pratica')

    #
    # menu configurazione
    #
    configurazione = timesheet.branch('!![it]Configurazione')

    configurazione.thpage('!![it]Sedi', table = 'timesheet.sede')
    configurazione.thpage('!![it]Operatori', table = 'timesheet.operatore')
    configurazione.thpage('!![it]Unità di misura', table = 'timesheet.unita_misura')
    configurazione.thpage('!![it]Categorie tipi lavoro', table = 'timesheet.categoria_lavoro')
    configurazione.thpage('!![it]Tipologie lavori', table = 'timesheet.tipo_lavoro')
    configurazione.thpage('!![it]Gruppi tariffari', table = 'timesheet.gruppo_tariffario')
