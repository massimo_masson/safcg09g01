#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('sede__id')
        r.fieldcell('operatore')
        r.fieldcell('operatore__id')
        r.fieldcell('pratica__id')
        r.fieldcell('anagrafica_cliente__id')
        r.fieldcell('descrizione')
        r.fieldcell('data')
        r.fieldcell('unita_misura__id')
        r.fieldcell('durata')
        r.fieldcell('tipo_lavoro__id')
        r.fieldcell('categoria_lavoro')
        #r.fieldcell('note')
        r.fieldcell('costo_tariffa')

    def th_order(self):
        return 'sede__id'

    def th_query(self):
        return dict(column='sede__id', op='contains', val='', runOnStart=True)

class ViewFromPratica(View):
    pass

class ViewFromAnagraficaCliente(View):
    pass

class ViewAnalisiRilevazioni(View):

    def th_groupedStruct(self, struct):
        u"Analisi per clienti"
        r = struct.view().rows()

        r.fieldcell('@anagrafica_cliente__id.caption', name=u'Cliente', width='20em')
        r.fieldcell('@tipo_lavoro__id.@categoria_lavoro__id.caption', name=u'Categoria', width='20em')
        r.fieldcell('@pratica__id.caption', name=u'Pratica', width='20em')
        r.fieldcell('@unita_misura__id.caption', name=u'U.M.', width='20em')
        r.fieldcell('durata', width='8em', group_aggr='sum', name='!![it]Durata', 
                dtype='N', format='#,##0.00;', totalize=True)
        r.fieldcell('costo_tariffa', width='8em', group_aggr='sum', name='!![it]C. tariffa', 
                dtype='N', format='#,##0.00;', totalize=True)
        r.cell('_grp_count', name='n.ril', width='5em', group_aggr='sum', dtype='L', childname='_grp_count')

    def th_groupedStruct_TipoLavoro(self, struct):
        u"Analisi per categorie attività"
        r = struct.view().rows()

        r.fieldcell('@tipo_lavoro__id.@categoria_lavoro__id.caption', name=u'Categoria', width='20em')
        r.fieldcell('@tipo_lavoro__id.caption', name=u'Tipo lavoro', width='20em')
        r.fieldcell('@anagrafica_cliente__id.caption', name=u'Cliente', width='20em')
        r.fieldcell('@pratica__id.caption', name=u'Pratica', width='20em')
        r.fieldcell('@unita_misura__id.caption', name=u'U.M.', width='20em')
        r.fieldcell('durata', width='8em', group_aggr='sum', name='!![it]Durata', 
                dtype='N', format='#,##0.00;', totalize=True)
        r.fieldcell('costo_tariffa', width='8em', group_aggr='sum', name='!![it]C. tariffa', 
                dtype='N', format='#,##0.00;', totalize=True)
        r.cell('_grp_count', name='n.ril', width='5em', group_aggr='sum', dtype='L', childname='_grp_count')


class Form(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=3, border_spacing='4px')
        fb.field('sede__id', hasDownArrow=True)
        fb.field('operatore')
        fb.field('operatore__id', hasDownArrow=True)
        # fb.checkBox('^.inidpendente_pratica', label='!![it]Selezioni indipendenti dalla pratica scelta', 
        #         tooltip='!![it]Cliente e attività non vincolati alle tipologia della pratica scelta')
        #fb.div()

        fb.field('pratica__id', hasDownArrow=True,
                #columns = '$cliente_riferimento__id',
                auxColumns = '@cliente_riferimento__id.ragione_sociale,@categoria_lavoro__id.descrizione',
                selected_cliente_riferimento__id = '.anagrafica_cliente__id',
                selected_tipo_lavoro_preferenziale__id = '.tipo_lavoro__id',
                selected_unita_misura_preferenziale__id = '.unita_misura__id'
                )
        fb.field('anagrafica_cliente__id', hasDownArrow=True, 
                condition = '$id = :cliente',
                condition_cliente = '=#FORM.record.@pratica__id.@cliente_riferimento__id.id'
                )
        fb.field('@pratica__id.@categoria_lavoro__id.caption', lbl='Categoria')

        fb.field('tipo_lavoro__id', hasDownArrow=True,
                condition = '$categoria_lavoro__id = :categoria', 
                condition_categoria = '=#FORM.record.@pratica__id.@categoria_lavoro__id.id',
                )
        fb.field('unita_misura__id', hasDownArrow=True)
        fb.field('durata')

        fb.field('data')
        fb.field('descrizione', colspan=2, width='100%')
        #fb.div()

        fb.field('note', colspan=3, width='100%',
                tag = 'simpleTextArea', editor = True,
                height = '5em'
                )

        # fb.field('cliente_caption')
        # fb.field('categoria_lavoro')


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px', duplicate=True)

class FormFromPratica(Form):
    pass

class FormFromAnagraficaCliente(Form):
    pass