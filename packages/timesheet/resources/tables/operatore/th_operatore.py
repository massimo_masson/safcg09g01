#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('codice')
        r.fieldcell('descrizione')
        r.fieldcell('user__id')
        r.fieldcell('gruppo_tariffario__id')
        r.fieldcell('@user__id.firstname')
        r.fieldcell('@user__id.lastname')
        r.fieldcell('tariffa')
        r.fieldcell('costo_standard')

    def th_order(self):
        return 'descrizione'

    def th_query(self):
        return dict(column='descrizione', op='contains', val='', runOnStart=True)



class Form(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=4, border_spacing='4px')
        
        fb.field('codice')
        #fb.div()
        fb.field('descrizione', colspan=3, width='100%')

        fb.field('user__id', hasDownArrow=True)
        # fb.field('@user__id.username', readOnly=True)
        # fb.field('@user__id.firstname', readOnly=True)
        # fb.field('@user__id.lastname', readOnly=True)
        fb.div(colspan=3)

        fb.field('gruppo_tariffario__id', hasDownArrow=True)
        fb.field('tariffa')
        fb.field('costo_standard')
        fb.div()


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
