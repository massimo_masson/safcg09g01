#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('codice_fiscale')
        r.fieldcell('partita_iva')
        r.fieldcell('ragione_sociale')

    def th_order(self):
        return 'codice_fiscale'

    def th_query(self):
        return dict(column='codice_fiscale', op='contains', val='', runOnStart=True)



class Form(BaseComponent):

    def th_form(self, form):
        #pane = form.record
        #fb = pane.formbuilder(cols=2, border_spacing='4px')
        #fb.field('codice_fiscale')
        #fb.field('partita_iva')
        #fb.field('ragione_sociale')
        #fb.div('')
        #fb.div('Più tutti gli altri campi di una bella anagrafica...')

        bc = form.center.borderContainer()
        self.anagraficaHeader(bc.contentPane(region = 'top', datapath = '.record'))
        self.anagraficaBody(bc.contentPane(region = 'center'))

    def anagraficaHeader(self, pane):
        fb = pane.formbuilder(cols = 2, border_spacing = '4px', width='80%')

        fb.field('codice_fiscale')
        fb.field('partita_iva')

        fb.field('ragione_sociale', colspan=2, width='100%')
        #fb.div('')

        fb.div('Più tutti gli altri campi di una bella anagrafica...', colspan=2, width='100%', background='lightgreen')

    def anagraficaBody(self, pane):
        tc = pane.tabContainer()

        # tab pratiche
        tab_pratiche = tc.contentPane(title = '!![it]Pratiche')
        tab_pratiche.dialogTableHandler(relation = '@pratiche_cliente',
                pbl_classes = True,
                viewResource = 'ViewFromAnagrafica',
                formResource = 'FormFromAnagrafica',
                grid_selfDragRows = True,
                margin = '2px',
                searchOn = True)

        # tab rilevazioni
        tab_rilevazioni = tc.contentPane(title = '!![it]Attività')
        tab_rilevazioni.dialogTableHandler(relation = '@rilevazioni',
                pbl_classes = True,
                viewResource = 'ViewFromAnagraficaCliente',
                formResource = 'FormFromAnagraficaCliente',
                grid_selfDragRows = True,
                margin = '2px',
                searchOn = True)

    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
