#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('codice')
        r.fieldcell('descrizione')
        r.fieldcell('cliente_riferimento__id')
        r.fieldcell('categoria_lavoro__id')
        r.fieldcell('cda_riferimento__id')
        r.fieldcell('data_inizio')
        r.fieldcell('data_fine_prevista')
        r.fieldcell('data_fine_effettiva')

    def th_order(self):
        return 'codice'

    def th_query(self):
        return dict(column='codice', op='contains', val='', runOnStart=True)

class ViewFromAnagrafica(View):
    pass

class Form(BaseComponent):

    def th_form(self, form):
        #pane = form.record
        #fb = pane.formbuilder(cols=1, border_spacing='4px')

        bc = form.center.borderContainer()
        self.praticaHeader(bc.contentPane(region = 'top', datapath = '.record'))
        self.praticaBody(bc.contentPane(region = 'center'))

    def praticaHeader(self, pane):
        fb = pane.formbuilder(cols = 3, border_spacing = '4px', width='80%')

        fb.field('codice')
        fb.field('descrizione', colspan=2, width='100%')

        fb.field('cda_riferimento__id')
        fb.field('cliente_riferimento__id', hasDownArrow=True, 
                colspan=2, width='100%'
                )

        fb.field('categoria_lavoro__id', hasDownArrow=True)
        fb.field('tipo_lavoro_preferenziale__id', hasDownArrow=True,
                condition = '$categoria_lavoro__id = :categoria', 
                condition_categoria = '=#FORM.record.@categoria_lavoro__id.id',
                )
        fb.field('unita_misura_preferenziale__id', hasDownArrow=True)

        fb.field('data_inizio')
        fb.field('data_fine_prevista')
        fb.field('data_fine_effettiva')

    def praticaBody(self, pane):
        tc = pane.tabContainer()

        # tab rilevazioni
        tab_rilevazioni = tc.contentPane(title = '!![it]Rilevazioni')
        tab_rilevazioni.dialogTableHandler(relation = '@rilevazioni',
                pbl_classes = True,
                viewResource = 'ViewFromPratica',
                formResource = 'FormFromPratica',
                grid_selfDragRows = True,
                margin = '2px',
                searchOn = True)


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')

class FormFromAnagrafica(Form):
    pass