#!/usr/bin/python3
# -*- coding: utf-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('categoria_lavoro__id')
        r.fieldcell('codice')
        r.fieldcell('descrizione')

    def th_order(self):
        return 'categoria_lavoro__id'

    def th_query(self):
        return dict(column='categoria_lavoro__id', op='contains', val='', runOnStart=True)



class Form(BaseComponent):

    def th_form(self, form):
        pane = form.record
        fb = pane.formbuilder(cols=2, border_spacing='4px')
        fb.field('categoria_lavoro__id', hasDownArrow=True)
        fb.field('codice')
        fb.field('descrizione')


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
