#!/usr/bin/env python
# encoding: utf-8
from gnr.app.gnrdbo import GnrDboTable, GnrDboPackage

class Package(GnrDboPackage):
    def config_attributes(self):
        return dict(comment='timesheet package',sqlschema='timesheet',sqlprefix=True,
                    name_short='Timesheet', name_long='Timesheet', name_full='Timesheet')
                    
    def config_db(self, pkg):
        pass
        
class Table(GnrDboTable):
    pass
