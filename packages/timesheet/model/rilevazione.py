#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# safcg09g01:timesheet 
# rilevazione tempi pratiche
# Copyright (C) 2021 Massimo Masson
# 
# This program is dual-licensed.
# 
# Option 1:
# If you respect the terms of GNU GPL license, AND
# you agree to give the copyright for modifications or derivative work
# to the original author Massimo Masson, the GPL license applies.
# In this case:
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# 
# Option 2:
# If you do not agree with any of the statements in option 1, then
# a proprietary license applies. In this case, contact the author
# for a dedicated propietary license.
# 

class Table(object):
    def config_db(self, pkg):
        '''rilevazione: righe registrate nel timesheet'''
        tbl = pkg.table('rilevazione', pkey = 'id', 
                name_long = '!![it]Rilevazione',
                name_plural = '!![it]Rilevazioni',
                caption_field = 'caption')

        self.sysFields(tbl)

        # RELAZIONE: sede
        sede__id = tbl.column('sede__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Sede',
                validate_notnull = True
                )
        sede__id.relation('timesheet.sede.id', mode = 'foreignkey',
                relation_name = 'rilevazioni',
                onDelete = 'raise'
                )

        # prima versione operatore non relazionato        
        tbl.column('operatore', dtype = 'A', size = ':22', 
                name_long = '!![it]Operatore OLD')

        # RELAZIONE: operatore
        operatore__id = tbl.column('operatore__id', size = '22',
                name_long = '!![it]Operatore',
                validate_notnull = True
                )
        operatore__id.relation('timesheet.operatore.id', mode = 'foreignkey',
                relation_name = 'rilevazioni',
                onDelete = 'raise'
                )

        # RELAZIONE: anagrafica cliente
        anagrafica_cliente__id = tbl.column('anagrafica_cliente__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Cliente',
                validate_notnull = True
                )
        anagrafica_cliente__id.relation('timesheet.anagrafica_cliente.id', mode = 'foreignkey',
                relation_name = 'rilevazioni',
                onDelete = 'raise'
                )

        # RELAZIONE: pratica
        pratica__id = tbl.column('pratica__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Pratica',
                #validate_notnull = True
                )
        pratica__id.relation('timesheet.pratica.id', mode = 'foreignkey',
                relation_name = 'rilevazioni',
                onDelete = 'raise'
                )
        
        tbl.column('operatore', dtype = 'A', size = ':22', 
                name_long = '!![it]Operatore')

        tbl.column('descrizione', dtype = 'A', size = ':256', 
                name_long = '!![it]Descrizione')

        tbl.column('data', dtype = 'D', name_long = '!![it]Data')

        # RELAZIONE: unità di misura
        unita_misura__id = tbl.column('unita_misura__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Unità di misura',
                validate_notnull = True
                )
        unita_misura__id.relation('timesheet.unita_misura.id', mode = 'foreignkey',
                relation_name = 'rilevazioni',
                onDelete = 'raise'
                )

        tbl.column('durata', dtype = 'N', 
                name_long = '!![it]Durata')

        # RELAZIONE: tipo_lavoro
        tipo_lavoro__id = tbl.column('tipo_lavoro__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Tipo lavoro',
                validate_notnull = True
                )
        tipo_lavoro__id.relation('timesheet.tipo_lavoro.id', mode = 'foreignkey',
                relation_name = 'rilevazioni',
                onDelete = 'raise'
                )
        
        tbl.column('note', dtype = 'A', size = ':1024', 
                name_long = '!![it]Note')

        # virtual columns
        tbl.formulaColumn('caption', 
                #'"("||$sede||") - "||$operatore||", "||$data||", " ||$cliente||", "||$pratica', name_long = '!![it]Titolo')
                "'('||$data||')'", name_long = '!![it]Titolo')

        tbl.formulaColumn('costo_tariffa',
                "$tariffa_operatore * $durata",
                name_long = '!![it]Costo a tariffa'
                )

        # alias 
        tbl.aliasColumn('cliente_caption', relation_path='@anagrafica_cliente__id.caption', name_long='!![it]Rif. cliente')

        tbl.aliasColumn('categoria_lavoro', relation_path='@tipo_lavoro__id.@categoria_lavoro__id.caption', 
                name_long='!![it]Categoria lavoro')

        tbl.aliasColumn('tariffa_operatore', 
                relation_path = '@operatore__id.tariffa', 
                name_long = '!![it]Tariffa operatore', name_short = 'T.op.'
                )

        tbl.aliasColumn('costo_standard_operatore', 
                relation_path = '@operatore__id.costo_standard',
                name_long = '!![it]Costo standard operatore', name_short = 'C.S.op.'
                )


    def defaultValues(self):
        return dict(
                data = self.db.workdate
                )