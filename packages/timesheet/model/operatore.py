#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# safcg09g01:timesheet 
# rilevazione tempi pratiche
# Copyright (C) 2021 Massimo Masson
# 
# This program is dual-licensed.
# 
# Option 1:
# If you respect the terms of GNU GPL license, AND
# you agree to give the copyright for modifications or derivative work
# to the original author Massimo Masson, the GPL license applies.
# In this case:
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# 
# Option 2:
# If you do not agree with any of the statements in option 1, then
# a proprietary license applies. In this case, contact the author
# for a dedicated propietary license.
# 

class Table(object):
    def config_db(self, pkg):
        '''operatore: operatore inserimento dato'''
        tbl = pkg.table('operatore', pkey = 'id', 
                name_long = '!![it]Operatore',
                name_plural = '!![it]Operatori',
                caption_field = 'caption')

        self.sysFields(tbl)

        # Per quale motivo un codice operatore in tabella?
        # In questo modo si possono creare utenti disgiunti dagli operatori genropy
        tbl.column('codice', dtype = 'A', size = ':32', 
                unmodifiable = True,
                unique = True, indexed = True, validate_notnull = True,
                name_long = '!![it]Codice operatore')

        tbl.column('descrizione', dtype = 'A', size = ':256', 
                name_long = '!![it]Descrizione operatore')

        # # RELAZIONE: genropy user
        user__id = tbl.column('user__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Utente di sistema',
                validate_notnull = False
                )
        user__id.relation('adm.user.id', mode = 'foreignkey',
                relation_name = 'operatori',
                onDelete = 'raise'
                )

        # RELAZIONE: gruppo tariffario
        gruppo_tariffario__id = tbl.column('gruppo_tariffario__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Gruppo tariffario',
                validate_notnull = True
                )
        gruppo_tariffario__id.relation('timesheet.gruppo_tariffario.id', mode = 'foreignkey',
                relation_name = 'operatori',
                onDelete = 'raise'
                )

        # virtual columns
        tbl.formulaColumn('caption', 
                "'('||$codice||') - '||$descrizione", name_long = '!![it]Titolo')

        # alias columns
        # tbl.aliasColumn('username',
        #         relation_path = '@user__id.username',
        #         name_long = '!![it]Nome accesso genropy'
        #         )

        # tbl.aliasColumn('firstname',
        #         relation_path = '@user__id.firstname',
        #         name_long = '!![it]Nome'
        #         )

        # tbl.aliasColumn('lastname',
        #         relation_path = '@user__id.lastname',
        #         name_long = '!![it]Cognome'
        #         )

        tbl.aliasColumn('tariffa',
                relation_path = '@gruppo_tariffario__id.tariffa',
                name_long = '!![it]Tariffa'
                )

        tbl.aliasColumn('costo_standard',
                relation_path = '@gruppo_tariffario__id.costo_standard',
                name_long = '!![it]Costo standard'
                )
