#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# safcg09g01:timesheet 
# rilevazione tempi pratiche
# Copyright (C) 2021 Massimo Masson
# 
# This program is dual-licensed.
# 
# Option 1:
# If you respect the terms of GNU GPL license, AND
# you agree to give the copyright for modifications or derivative work
# to the original author Massimo Masson, the GPL license applies.
# In this case:
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# 
# Option 2:
# If you do not agree with any of the statements in option 1, then
# a proprietary license applies. In this case, contact the author
# for a dedicated propietary license.
# 

class Table(object):
    def config_db(self, pkg):
        '''pratica: la pratica rappresenta un gruppo di attivita' correlate'''
        tbl = pkg.table('pratica', pkey = 'id', 
                name_long = '!![it]Pratica',
                name_plural = '!![it]Pratiche',
                caption_field = 'caption')

        self.sysFields(tbl)

        tbl.column('codice', dtype = 'A', size = ':32', 
                name_long = '!![it]Codice pratica')

        tbl.column('descrizione', dtype = 'A', size = ':256', 
                name_long = '!![it]Descrizione')

        # RELAZIONE: cliente_riferimento
        cliente_riferimento__id = tbl.column('cliente_riferimento__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Cliente di riferimento',
                #validate_notnull = True
                )
        cliente_riferimento__id.relation('timesheet.anagrafica_cliente.id', mode = 'foreignkey',
                relation_name = 'pratiche_cliente',
                onDelete = 'raise'
                )

        # RELAZIONE: categoria_lavoro
        categoria_lavoro__id = tbl.column('categoria_lavoro__id', size = '22',
                name_long = '!![it]Categoria lavoro',
                #unmodifiable = True,
                #validate_notnull = True
                )
        categoria_lavoro__id.relation('timesheet.categoria_lavoro.id', mode = 'foreignkey',
                relation_name = 'categorie_pratiche',
                onDelete = 'raise'
                )

        # RELAZIONE: tipo_lavoro_preferenziale
        tipo_lavoro_preferenziale__id = tbl.column('tipo_lavoro_preferenziale__id', size = '22',
                name_long = '!![it]Tipo lavoro preferenziale (più frequente)',
                )
        tipo_lavoro_preferenziale__id.relation('timesheet.tipo_lavoro.id', mode = 'foreignkey',
                relation_name = 'tipi_lavoro_preferenziale',
                onDelete = 'raise'
                )

        # RELAZIONE: unita_misura_preferenziale
        unita_misura_preferenziale__id = tbl.column('unita_misura_preferenziale__id', size = '22',
                name_long = '!![it]Unità di misura preferenziale (più frequente)',
                )
        unita_misura_preferenziale__id.relation('timesheet.unita_misura.id', mode = 'foreignkey',
                relation_name = 'unita_misura_preferenziale',
                onDelete = 'raise'
                )

        # da relazionare in futuro: centro di analisi
        cda_riferimento = tbl.column('cda_riferimento__id', size = '22',
                #unmodifiable = True,
                name_long = '!![it]Centro di analisi di riferimento',
                #validate_notnull = True
                )

        tbl.column('data_inizio', dtype = 'D', name_long = '!![it]Data inizio')

        tbl.column('data_fine_prevista', dtype = 'D', name_long = '!![it]Data fine prevista')

        tbl.column('data_fine_effettiva', dtype = 'D', name_long = '!![it]Data fine effettiva')

        # virtual columns
        tbl.formulaColumn('caption', 
                "'('||$codice||') - '||$descrizione", name_long = '!![it]Titolo')
