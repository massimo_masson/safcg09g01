#!/usr/bin/python3
# -*- coding: utf-8 -*-
# 
# safcg09g01:timesheet 
# rilevazione tempi pratiche
# Copyright (C) 2021 Massimo Masson
# 
# This program is dual-licensed.
# 
# Option 1:
# If you respect the terms of GNU GPL license, AND
# you agree to give the copyright for modifications or derivative work
# to the original author Massimo Masson, the GPL license applies.
# In this case:
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# 
# Option 2:
# If you do not agree with any of the statements in option 1, then
# a proprietary license applies. In this case, contact the author
# for a dedicated propietary license.
# 

class Table(object):
    def config_db(self, pkg):
        '''gruppo_tariffario: tariffa per gruppo'''
        tbl = pkg.table('gruppo_tariffario', pkey = 'id', 
                name_long = '!![it]Gruppo tariffario',
                name_short = '!![it]Grp.Tariffa',
                caption_field = 'caption')

        self.sysFields(tbl)

        tbl.column('codice', dtype = 'A', size = ':32', 
                name_long = '!![it]Codice gruppo tariffario')

        tbl.column('descrizione', dtype = 'A', size = ':128', 
                name_long = '!![it]Descrizione')
        
        tbl.column('tariffa', dtype = 'N', 
                name_long = '!![it]Tariffa')

        tbl.column('costo_standard', dtype = 'N', 
                name_long = '!![it]Costo standard')

        # virtual columns
        tbl.formulaColumn('caption', 
                "'('||$codice||') - '||$descrizione", name_long = '!![it]Titolo')
